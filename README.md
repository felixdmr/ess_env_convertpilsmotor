## 

---

This code has two scripts in it:
- convertMotorMaps
    - Creates and plots the motor efficiency maps from the excels provided by Johannes
- convertProfilesIntoPowerTrace
    - Dives into the bus data of the PiLS log out, finds the appropriate torque and rpm signals for each motor
    - Converts to electrical energy and applies motor efficiency maps, as well as applying 0.97 and 0.98 efficiency factors for inverter and PDU respectively
        - saved as profile.MotorSignals
    - Highlights the worst case individual motor signal with regards to energy use
        - as profile.MaxEnergyMotorProfile
    - Highlights average motor signal
        - as profile.AverageProfile
    - Highlights total power signal
        - as profile.TotPwProfile
    - Saves the total energy use (of all the motors combined) and duration of the entire profile
---

**ElePower_Pw** would be the power demand placed on the battery at the battery terminals. This is the one that really matters.

**ElePowerAtMotor_Pw** would be the power demand placed at the motor terminals.

**ShaftPower_Pw** would be the power demand on the motor shaft.

_T = Seconds

_W = rads/s

_Wrpm = rpm

_E = Joules

This follows the Battery Systems Modelling Conventions

--- 

Original Jira Ticket: [https://vertaero.atlassian.net/browse/VXBSM-367](https://vertaero.atlassian.net/browse/VXBSM-367)