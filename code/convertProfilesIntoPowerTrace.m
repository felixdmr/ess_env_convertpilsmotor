%n% Documentation
%   Name: convertProfilesIntoPowerTrace
%   Description: Script to convert given PiLS runs into a power trace
%   How to use:
%    - Adapt top level of script to the xlsx and their locations
%    - Press run
%   
%   Script is a bit dumb 
%       Assumes 8 motors, 8 subpacks.
%       Will output 
%

% Author and References
%{
Felix DMR
%}

clear
close all
clc

%% Locations and Settings
% motorEff = % Ignore, mapped in motorData
invertEff = 0.97; % Source: Johannes
pduEff = 0.98; % Source: worst case from sims run by Steven Fletcher

motorData = 'motorMap.mat';


% s and p for sub pack
cellsInS = 22*8;
cellsInP = 8;

% s and p for module
cellsInSMod = 22;
cellsInPMod = 8;

% location options
profilesFolder = "2022-03-18 PiL mission";
outputFolder = "output";

% plot options
plotProfiles = 1;
plotSummaries = 1;

% save options
saveFiles = 1;
savePlots = 1;
saveExcels = 1;

% time step interpolation
timeStep = 0.01;
excelTimeStep = 0.1;

%% Create output folder
if saveFiles || savePlots || saveExcels
    if ~isfolder(fullfile(outputFolder,profilesFolder))
        mkdir(fullfile(outputFolder,profilesFolder))
    end
end

%% load maps and data sources
load(motorData);

%% load traces
profiles = dir(fullfile("data", "profiles", profilesFolder, "*.mat"));
profilesSummary = struct;
profilesSummary.Names = string(vertcat(profiles.name));
profilesSummary.Energy_E = [];
profilesSummary.Duration_T = [];

for idx = 1:length(profiles)
    
    % load missions
    load(profiles(idx).name)
    
    disp("===============================================")
    disp(profiles(idx).name)
    disp("===============================================")
    
    % create temp var to hold epu
    temp = out.logsout{1}.Values.flight_dynamics.aircraft_states.epuBus;
    
    % create subset of signals of just the torque and the speed for each motor
    % disregard other signals, not useful
    motorSignals(8) = struct; 
    energyArray = zeros(1,8);
    totalEnergy = 0;

    % create average motor profile
    sumProfile = struct;
    
    % Iterate over individual motors
    for motorNum = 1:8
        % ==========================================
        %% Parse and interpolate signal
        origTime = temp.("speed_motor_rpm_M" + num2str(motorNum)).Time;
        newTime = [min(origTime):timeStep:max(origTime)]';

        % Time
        motorSignals(motorNum).TimeData_T = newTime;        

        % RPM
        motorSignals(motorNum).Speed_Wrpm = interp1(origTime, ...
                 temp.("speed_motor_rpm_M" + num2str(motorNum)).Data, ...
                 newTime);

        % Rad/s
        motorSignals(motorNum).Speed_W = interp1(origTime, ...
                 temp.("speed_motor_rads_M" + num2str(motorNum)).Data, ...
                 newTime);

        % Torque
        motorSignals(motorNum).Torque_Tq = interp1(origTime, ...
                 temp.("torque_motor_Nm_M" + num2str(motorNum)).Data,...
                newTime);

        % POwer 
        motorSignals(motorNum).ShaftPower_Pw = ....
            motorSignals(motorNum).Torque_Tq .* ....
            motorSignals(motorNum).Speed_W;
        
        %% Dervive new signals
        % interpolate signals
        % Vq = interp2(X,Y,V,Xq,Yq)
        motorSignals(motorNum).ElePowerAtMotor_Pw = interp2(motor.Speed_Wrpm, ... X
            motor.Torque_Tq, ... Y
            squeeze(motor.ElePower_Pw(1,:,:)),... V
            motorSignals(motorNum).Speed_Wrpm, ... Xq
            motorSignals(motorNum).Torque_Tq); % Yq
        
        % apply further efficiency
        motorSignals(motorNum).ElePower_Pw = (motorSignals(motorNum).ElePowerAtMotor_Pw ./ invertEff) ./ pduEff;


        
        % calc net energy usage
        motorSignals(motorNum).NetEnergy_E = cumtrapz(motorSignals(motorNum).TimeData_T, motorSignals(motorNum).ElePower_Pw);
        
        %% Add to Average profile 
        % ============================================
        % Add to the average motor profile
        if motorNum == 1
            % if first time, get field names from first motor profile
            if idx == 1
                signalNames = fieldnames(motorSignals(motorNum));
            end

            % create sumProfile 
            for idxSigName = 1:length(signalNames)
                sumProfile.(signalNames{idxSigName}) = ...
                    motorSignals(motorNum).(signalNames{idxSigName});
            end
        else
            % sum into sumprofile (for totals and avg)
            for idxSigName = 1:length(signalNames)
                sumProfile.(signalNames{idxSigName}) = ...
                    sumProfile.(signalNames{idxSigName}) + ...
                    motorSignals(motorNum).(signalNames{idxSigName});
            end

        end
            
    
        %%  Create Motors Summary values
        motorSignals(motorNum).FinalEnergy_E = trapz(motorSignals(motorNum).TimeData_T, motorSignals(motorNum).ElePower_Pw);
        motorSignals(motorNum).MaxPower_Pw = max(motorSignals(motorNum).ElePower_Pw);
        motorSignals(motorNum).MinPower_Pw = min(motorSignals(motorNum).ElePower_Pw);
        motorSignals(motorNum).MeanPower_Pw = mean(motorSignals(motorNum).ElePower_Pw);
        motorSignals(motorNum).MedianPower_Pw = median(motorSignals(motorNum).ElePower_Pw);
        motorSignals(motorNum).StdPower_Pw = std(motorSignals(motorNum).ElePower_Pw);
        % Counted values
        energyArray(motorNum) = motorSignals(motorNum).FinalEnergy_E;
        totalEnergy = totalEnergy + motorSignals(motorNum).FinalEnergy_E;
        
        disp("Motor ")
        disp("Total Energy: " +  ...
            motorSignals(motorNum).FinalEnergy_E / (1000* 1000) + ...
            "MJ    Max Power: " +...
            motorSignals(motorNum).MaxPower_Pw / 1000 + 'kW')
    
    end
    
    % ============================================
    % create top level summary
    profile = struct;
    profile.Name = profiles(idx).name;
    profile.MotorSignals = motorSignals;

    % Create Total Power profiles
    profile.TotPwProfile.TimeData_T = profile.MotorSignals(1).TimeData_T;
    profile.TotPwProfile.ShaftPower_Pw = sumProfile.ShaftPower_Pw;
    profile.TotPwProfile.ElePower_Pw = sumProfile.ElePower_Pw;
    profile.TotPwProfile.NetEnergy_E = sumProfile.NetEnergy_E;

    % Create Average Profile
    for idxSigName = 1:length(signalNames)
        profile.AverageProfile.(signalNames{idxSigName}) = ...
            sumProfile.(signalNames{idxSigName}) ./ 8;
    end
    
    

    % Create worst profile
    [~, profile.MaxEnergyMotor] = max(energyArray);
    profile.MaxEnergyMotorProfile = profile.MotorSignals(profile.MaxEnergyMotor);
    
    profile.TotalEnergy_E = sum(horzcat(profile.MotorSignals(:).FinalEnergy_E));
    profile.Duration_T = profile.MaxEnergyMotorProfile.TimeData_T(end);

    disp("Motor Signal with largest energy draw within Mission: " + ...
        profile.MaxEnergyMotor)
    disp("Total Energy draw for all motors: " + ...
        profile.TotalEnergy_E / (1000*1000) + "MJ")
    disp("Mission Duration: " + ...
        profile.Duration_T/60 + " mins")
    clear motorSignals energyArray totalEnergy
    
    % add to summaries
    profilesSummary.Energy_E(idx) = profile.TotalEnergy_E;
    profilesSummary.Duration_T(idx) = profile.Duration_T;

    % ============================================
    % Decide if to plot and save plots
    if plotProfiles
        plotProfile(profile);
        if savePlots
            figName = strsplit(profiles(idx).name, '.');
            figName{end} = 'fig';
            figname = strjoin(figName, '.');
            savefig(fullfile(outputFolder,profilesFolder,figname));
        end

        plotProfileAvg(profile);
        if savePlots
            savefig(fullfile(outputFolder,profilesFolder,("TotAvg_" + figname)));
        end

    end

    % decide if to save .mat files
    if saveFiles
        if ~isfolder(fullfile(outputFolder,profilesFolder))
            mkdir(fullfile(outputFolder,profilesFolder))
        end
        save(fullfile(outputFolder,profilesFolder,("converted_" + profiles(idx).name)), "profile")
    end

    %% Save To Excel
    % decide if to saveExcel
    if saveExcels
        % create tables from Max Energy Motor Profile
        selectedProfile = "MaxEnergyMotorProfile";

        tempTime = profile.(selectedProfile).TimeData_T;
        tempData = profile.(selectedProfile).ElePower_Pw;
        ti = [min(tempTime):excelTimeStep:max(tempTime)]' ;
        interpolatedData = interp1(tempTime,tempData,ti) ;

        figure
            plot(tempTime, tempData)
            hold on; grid minor
            plot(ti, interpolatedData)

        subpackTable = table(ti,...
            interpolatedData,...
            'VariableNames',{'TimeData [s]', 'PowerDraw [W]'});
        moduleTable = table(ti,...
            interpolatedData ./ 8,...
            'VariableNames',{'TimeData [s]', 'PowerDraw [W]'});

        informationTable = {"Date Produced:", string(datetime)
                            "Profile:", profiles(idx).name
                            "Profile Group:", profilesFolder
                            "Subpack Cells in Series:", cellsInS
                            "Subpack Cells in Parallel:", cellsInP
                            "Module Cells in Series: ", cellsInSMod
                            "Module Cells in Parallel: ", cellsInPMod
                            "Selected profile method:", selectedProfile};

        % create filename
        excelFile = strsplit(profiles(idx).name, '.');
        excelFile{end} = 'xlsx';
        excelFile = strjoin(excelFile, '.');
        excelFile = fullfile(outputFolder,profilesFolder,excelFile);

        % Write tables
        writecell(informationTable, excelFile,"Sheet","Information")
        writetable(moduleTable, excelFile, "Sheet","Module")
        writetable(subpackTable, excelFile, "Sheet","Subpack")
    
    %% END

    end

end

%% Summaries
% plot summaries
if plotSummaries
    plotSummary(profilesSummary);
end

% Save summaries
if saveFiles
    save(fullfile(outputFolder,profilesFolder,"profilesSummary"), "profilesSummary")
end

%% Functions
function [] = plotSummary(profilesSummary)
    figure
        y = vertcat(profilesSummary.Energy_E ./ (1000 * 1000), profilesSummary.Duration_T);
        x = profilesSummary.Names;
        xCat = categorical(x);
        xCat = reordercats(xCat, x);
        bar(xCat, y')
        set(gca,'TickLabelInterpreter','none')
        title("Duration and Total Energy use for Selected Profiles")
        legend("Energy Use [MJ]", "Duration [s]")


end



% signals = {};
% for i = 1:8
%     signal{i} = ["out.logsout{1}.Values.flight_dynamics.aircraft_states.epuBus" + "power_electrical_W_M" + num2str(i)];
% end