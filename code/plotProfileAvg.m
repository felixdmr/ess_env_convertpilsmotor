function [fig] = plotProfileAvg(profile)
%PLOTPROFILE Function which plots the profile signals
%   Name: plotProfileAvg
%   Description: Plots profile signals for Total, Avg and Worst case motor
%   loading on battery
%   How to use:
%    - Input profule
%   
%

% Author and References
%{
Felix DMR
%}

fig = figure("WindowState", "maximized");
    subplot(2,1,1) % Plot Tot, Worst, Avg
            plot(profile.TotPwProfile.TimeData_T, ...
                profile.TotPwProfile.ElePower_Pw ./ (1000))
            hold on
            grid minor
            plot(profile.AverageProfile.TimeData_T, ...
                profile.AverageProfile.ElePower_Pw ./ (1000))
            plot(profile.MaxEnergyMotorProfile.TimeData_T, ...
                profile.MaxEnergyMotorProfile.ElePower_Pw ./ (1000))
            

            xlabel('Time [s]')
            ylabel('Power Draw [kW]')
            title("Comparison of Power Draws in " + profile.Name, 'Interpreter','none')
            legend("Total Draw", "Average Draw", "Most Loaded Motor Draw")      

    
    subplot(2,1,2) % All Net Energies
        plot(profile.TotPwProfile.TimeData_T, ...
                profile.TotPwProfile.NetEnergy_E ./ (1000 * 1000))
            hold on
            grid minor
            plot(profile.AverageProfile.TimeData_T, ...
                profile.AverageProfile.NetEnergy_E ./ (1000 * 1000))
            plot(profile.MaxEnergyMotorProfile.TimeData_T, ...
                profile.MaxEnergyMotorProfile.NetEnergy_E ./ (1000 * 1000))
            

            xlabel('Time [s]')
            ylabel('Net Energy Draw  [MJ]')
            title("Comparison of Power Draws in " + profile.Name, 'Interpreter','none')
            legend("Total Draw", "Average Draw", "Most Loaded Motor Draw")   
            
end

