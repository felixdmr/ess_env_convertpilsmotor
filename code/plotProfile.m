function [fig] = plotProfile(profile)
%PLOTPROFILE Function which plots the profile signals
%   Name: plotProfile
%   Description: Script to convert given PiLS runs into a power trace
%   How to use:
%    - Adapt top level of script to the xlsx and their locations
%    - Press run
%   
%   Script is a bit dumb 
%       Assumes 8 motors, 8 subpacks.
%       Will output 
%

% Author and References
%{
Felix DMR
%}

fig = figure("WindowState", "maximized");
    subplot(3,1,1) % All Battery Powers
        for motorNum = 1:8
            plot(profile.MotorSignals(motorNum).TimeData_T,...
                profile.MotorSignals(motorNum).ElePower_Pw ./ 1000)
            if motorNum == 1
                hold on
                grid minor
            elseif motorNum == 8
                xlabel('Time [s]')
                ylabel('Power Draw at Battery [kW]')
                title("Power Draw for All Motors in " + profile.Name, 'Interpreter','none')
                legend("Motor " + [1:8])
            end
        end
    
    subplot(3,1,2) % All Net Energies
        for motorNum = 1:8
            plot(profile.MotorSignals(motorNum).TimeData_T, ...
                profile.MotorSignals(motorNum).NetEnergy_E ./ (1000 * 1000))
            if motorNum == 1
                hold on
                grid minor
            elseif motorNum == 8
                xlabel('Time [s]')
                ylabel('Net Energy Draw at Battery [MJ]')
                title("Net Energy Draw for All Motors in " + profile.Name, 'Interpreter','none')
                legend("Motor " + [1:8])
            end
        end

    subplot(3,1,3) % Shaft Power + Elec + Battery for worst case
            plot(profile.MaxEnergyMotorProfile.TimeData_T, ...
                profile.MaxEnergyMotorProfile.ShaftPower_Pw ./ (1000))
            hold on
            grid minor
            plot(profile.MaxEnergyMotorProfile.TimeData_T, ...
                profile.MaxEnergyMotorProfile.ElePowerAtMotor_Pw ./ (1000))
            plot(profile.MaxEnergyMotorProfile.TimeData_T, ...
                profile.MaxEnergyMotorProfile.ElePower_Pw ./ (1000))
            

                xlabel('Time [s]')
                ylabel('Power Draw [kW]')
                title("Comparison of Power Draws for Worst Case Motor in " + profile.Name, 'Interpreter','none')
                legend("Calculated Shaft Power", "Estimated Motor Electrical Power", "Estimated Battery Electrical Power")      

            
end

