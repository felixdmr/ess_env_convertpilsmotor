%% Documentation
%   Name: convertMotorMaps
%   Description: Script to convert given motor map excels into a neater .
%   How to use:
%    - Adapt top level of script to the xlsx and their locations
%    - Press run
%   
%   Script is a bit dumb and assumes individual maps are for different
%   voltages
%

% Author and References
%{
Felix DMR

%}
clear
close all
clc


%% Locations and Settings
dataFolder = fullfile('data','motor');

% plot options
plotEffSurf = 1;
plotEffDelta = 1;
plotShaftVsEleVsCalc = 1;
plotDiffToEfficiency = 1;

% save options
saveFile =1;


%% pull all data
motorMaps = dir(fullfile(dataFolder,"*.xlsx"));

varsAndSheets = {'Speed_Wrpm', 'Speed';
                'Torque_Tq', 'Shaft_Torque';
                'Shaft_PwkW', 'Shaft_Power'
                'Efficiency_Pc', 'Efficiency';
                'Electric_PwkW', 'Electromagnetic_Power';
                'Terminal_PwkW', 'Terminal_Power';
                'DCVoltage_U', 'DC_Bus_Voltage';
                'DCCurrent_I', 'Idc'};

temp(length(motorMaps)) = struct;
for j = 1:length(motorMaps)
    for i = 1:size(varsAndSheets, 1)
        temp(j).(varsAndSheets{i,1}) = readmatrix(...
                        fullfile(motorMaps(j).folder, motorMaps(j).name),...
                        "Sheet",varsAndSheets{i,2});
    end
end


%% Convert to map
motor = struct;

% RPM
% check maps match
for idx = 2:length(motorMaps)
    if max(temp(1).Speed_Wrpm) ~= max(temp(idx).Speed_Wrpm)
        warning(["Speed map of " motorMaps(1).name + " does not match " + motorMaps(idx).name + "."])
    end
end

% take the top rpm for each column
motor.Speed_Wrpm = max(temp(1).Speed_Wrpm);
motor.Speed_W = motor.Speed_Wrpm * 2 * pi() / 60;

% torque
motor.Torque_Tq = round(median(temp(1).Torque_Tq'));
% check maps match
for idx = 2:length(motorMaps)
    if motor.Torque_Tq ~= round(median(temp(idx).Torque_Tq'))
        warning("Torque map of " + motorMaps(1).name + " does not match " + motorMaps(idx).name + ".")
    end
end

% Voltage
% Might need to add a sort here if voltages are not in order
motor.Voltage_U = zeros(1,length(motorMaps));
for idx = 1:length(motorMaps)
    motor.Voltage_U(idx) = round(median(temp(idx).DCVoltage_U,"all"));
end



% Temperature

% Efficiency
motor.Efficiency_Pc = zeros(length(motor.Voltage_U), length(motor.Torque_Tq), length(motor.Speed_W));
for idx = 1:length(motor.Voltage_U)
    motor.Efficiency_Pc(idx,:,:) = temp(idx).Efficiency_Pc;
end

% Shaft Power
motor.ShaftPower_Pw = zeros(length(motor.Voltage_U), length(motor.Torque_Tq), length(motor.Speed_W));
for idx = 1:length(motor.Voltage_U)
    motor.ShaftPower_Pw(idx,:,:) = temp(idx).Shaft_PwkW * 1000;
end

% Electric Power Demand % 
motor.ElePower_Pw = zeros(length(motor.Voltage_U), length(motor.Torque_Tq), length(motor.Speed_W));
for idx = 1:length(motor.Voltage_U)
    motor.ElePower_Pw(idx,:,:) = temp(idx).Terminal_PwkW * 1000;
end

% current demand
motor.Current_I = zeros(length(motor.Voltage_U), length(motor.Torque_Tq), length(motor.Speed_W));
for idx = 1:length(motor.Voltage_U)
    motor.Current_I(idx,:,:) = temp(idx).DCCurrent_I;
end

% Back calculated electric power 
% Electric Power Demand % 
motor.ElePower2_Pw = zeros(length(motor.Voltage_U), length(motor.Torque_Tq), length(motor.Speed_W));
for idx = 1:length(motor.Voltage_U)
    motor.ElePower2_Pw(idx,:,:) = temp(idx).DCCurrent_I * motor.Voltage_U(idx);
end



% Ignore electromagnetic power
% % Terminal Power Demand
% motor.TerminalPower_Pw = zeros(length(motor.Voltage_U), length(motor.Torque_Tq), length(motor.Speed_W));
% for idx = 1:length(motor.Voltage_U)
%     motor.TerminalPower_Pw(idx,:,:) = temp(idx).Terminal_PwkW * 1000;
% end

%% Plot
subplotNum = length(motorMaps);


% plot efficiency surface
contourSteps = (1 - ([1:-0.05:0] .^ 4)) * 100;

if plotEffSurf
figure("WindowState", "maximized")
    for idx = 1:subplotNum
        subplot(1,subplotNum,idx)
            contourf(motor.Speed_Wrpm, motor.Torque_Tq, squeeze(motor.Efficiency_Pc(idx,:,:)), contourSteps);
            xlabel("Speed\_Wrpm")
            ylabel("Torque\_Tq")
            title("Efficiency Map [%]at " + motor.Voltage_U(idx) + " Volts")
            if idx == subplotNum
                colorbar
            end

    end
end % if plotEffSurf

% plot efficiency deltas
if plotEffDelta
figure("WindowState", "maximized")
    for idx = 2:subplotNum
        subplot(1,subplotNum-1,idx-1)
            contourf(motor.Speed_Wrpm, motor.Torque_Tq, squeeze(motor.Efficiency_Pc(idx,:,:)-motor.Efficiency_Pc(1,:,:)), [-5,-1,0,1,5,100]);
            xlabel("Speed\_Wrpm")
            ylabel("Torque\_Tq")
            title("Efficiency Map Delta [%] at " + motor.Voltage_U(idx) + " Volts with regards to " + motor.Voltage_U(1))
            if idx == subplotNum
                colorbar
            end
    end
end % if plotEffDelta

 
% plot shaft power vs electric power vs calculated power
if plotShaftVsEleVsCalc
    for idx = 1:subplotNum
        figure("WindowState", "maximized")
                subplot(1,3,1)
                    contourf(motor.Speed_Wrpm, motor.Torque_Tq, squeeze(motor.ShaftPower_Pw(idx,:,:)./1000));
                    xlabel("Speed\_Wrpm")
                    ylabel("Torque\_Tq")
                    title("Shaft Power Map [kW] at " + motor.Voltage_U(idx) + ".")
                    colorbar
        
                subplot(1,3,2)
                    contourf(motor.Speed_Wrpm, motor.Torque_Tq, squeeze(motor.ElePower_Pw(idx,:,:)./1000));
                    xlabel("Speed\_Wrpm")
                    ylabel("Torque\_Tq")
                    title("Electric Power Map [kW] at " + motor.Voltage_U(idx) + "." )
                    colorbar
        
                subplot(1,3,3)
                    contourf(motor.Speed_Wrpm, motor.Torque_Tq, squeeze(motor.ElePower2_Pw(idx,:,:)./1000));
                    xlabel("Speed\_Wrpm")
                    ylabel("Torque\_Tq")
                    title("Calculated Power Map [kW] at " + motor.Voltage_U(idx) + ".")
                    colorbar
    end
end % if plotShaftVsEleVsTerm

% plot difference in between shaft power divided by efficiency and Electric
% and Terminal maps
if plotDiffToEfficiency
for idx = 1:subplotNum
    figure("WindowState", "maximized")
            subplot(1,2,1)
                contourf(motor.Speed_Wrpm, motor.Torque_Tq, squeeze(...
                    (motor.ElePower_Pw(idx,:,:) ) - ...
                    (motor.ShaftPower_Pw(idx,:,:)) ./ ...
                    (motor.Efficiency_Pc(idx,:,:)./100)));
                xlabel("Speed\_Wrpm")
                ylabel("Torque\_Tq")
                title("Electric Power Map Delta [W] To Shaft / Efficiency for " + motor.Voltage_U(idx) + ".")
                colorbar
    
            subplot(1,2,2)
                contourf(motor.Speed_Wrpm, motor.Torque_Tq, squeeze(...
                    (motor.ElePower2_Pw(idx,:,:) ) - ...
                    (motor.ShaftPower_Pw(idx,:,:)) ./ ...
                    (motor.Efficiency_Pc(idx,:,:)./100)));
                xlabel("Speed\_Wrpm")
                ylabel("Torque\_Tq")
                title("Calculated DC Power Map Delta To [W] Shaft / Efficiency for " + motor.Voltage_U(idx) + ".")
                colorbar
end
end % if plotDiffToEfficiency

%% Save files
if saveFile
    save(fullfile(dataFolder, "motorMap.mat"), 'motor')
end